import os
import math
import gym
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions import Categorical
import argparse
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
from minatar import Environment
import numpy as np
import matplotlib.pyplot as plt
import copy
from lunar_lander_heuristic import Heuristic_agent, Decision_tree_agent
from sklearn import cluster
from shapley import estimate_shapley
from utils.math import *
# from config import *
from IPython import display
from utils.env import make_env
from utils.gym_wrapper import GymWrapper
from copy import deepcopy
from forward_dynamics import ForwardDynamics
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

from policy_analysis_interact import * 

# def collect_obs(policy, env, max_epi=100, max_steps=1000):
#     obs_lists=[]
#     for epi in range(max_epi):
#         o = env.reset()
#         print('Current episode: {}'.format(epi))
#         for step in range(max_steps):
#             obs_lists.append(o)
#             a, _ = policy.choose_action(o)
#             o_, r, done, _ = env.step(a)
#             o = o_
#             if done: break
#         print(step)
#     obs_lists = np.array(obs_lists)
#     if Continuous:
#         np.save('observations_continuous', obs_lists)
#     else:
#         np.save('observations_discrete', obs_lists)
#     return obs_lists


# def features_statistics(o):
#     """
#     Make distribution of observations of policy rollouts on each dimension of samples.
#     Input: (N, D), where N is the number of samples, and D is the dimension of each sample.
#     Return: list of modes and list of means for each dimension.
#     """
#     o = np.swapaxes(o, 0,1)
#     dim = o.shape[0]
#     mode_list = []
#     mean_list = []
#     for idx in range(dim):
#         mean = np.mean(o[idx])
#         hist, edges = np.histogram(o[idx], bins=100)
#         max_bin=np.argmax(hist)
#         mode=edges[max_bin]
#         mode_correction = 0.5 *(edges[1] - edges[0])
#         mode+=mode_correction  # half bin correction
#         # print('{:.4f}'.format(mode))
#         mode_list.append(mode)
#         mean_list.append(mean)
#     return mode_list, mean_list

# def clustered_features_statistics(o, n_clusters, num_bins=40):
#     """
#     Make distribution of observations of policy rollouts on clusters (along feature dimension) of samples.
#     Attribute:
#         n_clusters: number of clusters for features
#     Return: list of modes for each dimension (derived in joint manner).
#     """
#     def local_affinity(x):
#         return 1-abs(np.corrcoef(x))

#     # feature clustering
#     agglo = cluster.FeatureAgglomeration(n_clusters=n_clusters, linkage='average', affinity=local_affinity)
#     labels = agglo.fit(o)
#     cluster_indices = np.array(agglo.labels_)
#     print('Cluster indices for all features: ',cluster_indices)

#     feature_clusters=[]
#     for i in range(n_clusters):
#         feature_clusters.append(np.array(np.where(cluster_indices==i)).reshape(-1))

#     def argmax(X):  # argmax of N dim array
#         l = np.unravel_index(X.argmax(), X.shape)
#         return l

#     mode_list=[]

#     for c in feature_clusters:
#         c = c.tolist()
#         selected_obs = o[:, c]
#         correction = 1./(2*num_bins)  # the correction of half of the bin for values based on edges
#         if len(c)==1:
#             max_bin = np.argmax(selected_obs)
#             mode = selected_obs[max_bin]+correction
#             # mode_list = np.concatenate((mode_list, mode))
#             mode_list.append(mode)
#         elif len(c)>=2:
#             hist, edges = np.histogramdd(selected_obs, bins = np.shape(selected_obs)[-1]*(num_bins, ))
#             max_bin = argmax(hist)
#             mode=[]
#             for i in range(len(max_bin)):
#                 mode.append(edges[i][max_bin[i]]+correction)
#             # mode_list = np.concatenate((mode_list, mode))
#             mode_list.append(mode)
#     # feature_clusters_flatten = np.concatenate(feature_clusters)  # list of arrays to 1d array

#     # sort the mode list according to the order o66f features, as clustering perturbs original order of features
#     # sorting_order = np.array(feature_clusters_flatten).argsort()
#     # sorted_mode_list = np.array(mode_list)[sorting_order]
#     # return sorted_mode_list.tolist()

#     return [feature_clusters,  mode_list]   # list of feature clusters indices, e.g. [[i1, i2], [i3]], list of correponding modes, e.g. [[m1, m2], [m3]]


# def load_data(policy, env):
#     data_prefix  = './data/'
#     if Agent == 'Heuristic':
#         if Continuous:
#             obs = np.load(data_prefix+'observations_continuous_heuristic.npy')
#         else:
#             obs = np.load(data_prefix+'observations_discrete_heuristic.npy')

#     elif Agent == 'PPO':
#         if Continuous:
#             obs = np.load(data_prefix+'observations_continuous.npy')
#         else:
#             obs = np.load(data_prefix+'observations_discrete.npy')

#     elif Agent == 'Decision_tree':
#         if Continuous:  # not implemented yet
#             try:
#                 obs = np.load(data_prefix+'observations_continuous_decisiontree{}.npy'.format(Tree_depth))
#             except:
#                 obs = collect_obs(policy, env)
#         else:
#             try:
#                 obs = np.load(data_prefix+'observations_continuous_decisiontree{}.npy'.format(Tree_depth))
#             except:
#                 obs = collect_obs(policy, env)
#     return obs

# def get_perturbation_feature_value(data, replacement="Mode"):
#     if Perturbation == 'Random':
#         mode_list = None
#         mean_list = None
#     else:
#         # for Perturbation == 'Single'
#         mode_list, mean_list = features_statistics(data)

#         if Perturbation=='Cluster':  # do we need mean for clustering perturbation? right now it just uses mean of single perturbation
#             mode_list = clustered_features_statistics(data, n_clusters=Num_clusters)
#             mean_list =  [mode_list[0],  [np.array(mean_list)[l] for l in mode_list[0]]]  # cluster mean values in the same way as the mode

#     if replacement ==  'Mode':
#         perturbation_feature_values = mode_list
#     elif replacement ==  'Mean':
#         perturbation_feature_values = mean_list
#     else:
#         pass
#     return perturbation_feature_values

# def perturb_features(o, perturbed_features):
#     """
#     Disturb the features for each dimension.
#     """
#     perturbed_o=[]
#     dim = np.array(o).shape[-1]
#     original_o = o.copy()
#     binary_dim=[6,7]  # LunarLander 
#     if Perturbation is 'Cluster':
#         [feature_clusters,  perturbation_values] = perturbed_features
#         for indices, values in zip(feature_clusters, perturbation_values):
#             o[indices] = values

#             # flip value for binary
#             for dim in binary_dim:
#                 if dim in indices:
#                     o[dim] = 0. if original_o[dim]>0 else 1.
#             perturbed_o.append(o)
#             o = original_o.copy()
#     else:
#         for i in range(dim):
#             if Perturbation is 'Random': # if random perturbation, replace it with random values from normal distribution
#                 if i in binary_dim:
#                     if np.random.normal()>0.5:
#                         o[i] = 1
#                     else:
#                         o[i] = 0
#                 else:
#                     o[i] = np.random.normal()  # here should be range of state dimension
#             elif Perturbation is 'Single':
#                 if i in binary_dim:  # dim 6, 7, flip for binary
#                     o[i] = 0. if o[i]>0 else 1.
#                 else:
#                     o[i] = perturbed_features[i]
#             else:
#                 raise NotImplementedError  
#             perturbed_o.append(o)
#             o = original_o.copy()
#     return perturbed_o

# def value_function_measure(env, v_func, forward_dynamics, o, a1, dist1, a2, dist2, N=10):
#     """
#     Get the policy difference via the value function of next states.
#     o1 -> a1; o1, a1 -> o1' -> Q(o1, a1)=r(o1, a1)+gamma*V(o1')
#     o2 -> a2; o1, a2 -> o2' -> Q(o1, a2)=r(o1, a2)+gamma*V(o2') 
#     diff = Q(o1, a1) - Q(o1, a2)

#     Arguments:
#     ----------
#     N: number of actions sampled from the stochastic policy
#     """
#     if dist1 and dist2:  # stochastic
#         q1_list=[]
#         q2_list=[]
#         for i in range(N):
#             # Here we want different rollouts from env starting with the same state but 
#             # different actions. Depending on whether the environment is duplicable, there 
#             # are two approaches: 

#             # 1. If env is duplicable, we directly duplicate the env and rollout.

#             # env1 = deepcopy(env)
#             # env2 = deepcopy(env) 
#             # o1,r1,_,_ = env1.step(a1)
#             # o2,r2,_,_ = env1.step(a2)
#             # env1.close()
#             # env2.close()

#             # 2. If the env is not duplicable, we need a pre-trained forward model to provide "rollouts".
#             # the env LunarLander is not duplicated correctly here, since the Box2D and Classic Control
#             # envs in OpenAI Gym can not be simply copied. Atari, Mujoco and Robotics can be 
#             # duplicated through recording and resetting the state.  
#             # Since env cannot be duplicated, we use a learned forward dynamics model to predict next state.
#             o_a1 = torch.Tensor(np.concatenate((o, a1)))
#             o_a2 = torch.Tensor(np.concatenate((o, a2)))
#             r_o1 = forward_dynamics(o_a1).detach().cpu().numpy()
#             r_o2 = forward_dynamics(o_a2).detach().cpu().numpy()
#             def split_dims(data, dim):
#                 x1 = data[:dim]
#                 x2 = data[dim:]
#                 return x1, x2
#             r1, o1 = split_dims(r_o1, dim=1)
#             r2, o2 = split_dims(r_o2, dim=1)

#             v1 = v_func(o1)
#             v2 = v_func(o2)
#             if torch.is_tensor(v1):
#                 v1 = v1.detach().cpu().numpy()
#                 v2 = v2.detach().cpu().numpy()
#             q1 = r1/R_Norm+Gamma*v1  # R_Norm is factor for normalization during prediction
#             q2 = r2/R_Norm+Gamma*v2

#             q1_list.append(q1)
#             q2_list.append(q2)

#         diff = np.mean(q1_list) - np.mean(q2_list)  # it can be negative in practice, which should be avoided!

#     else:  # deterministic
#         env1 = deepcopy(env)
#         env2 = deepcopy(env)
#         o1, r1, _, _ = env1.step(a1)
#         o2, r2, _, _ = env2.step(a2)
#         v1 = v_func(o1)
#         v2 = v_func(o2)
#         q1 = r1+Gamma*v1
#         q2 = r2+Gamma*v2
#         diff = q1 - q2
#         env1.close()
#         env2.close()

#     return diff

# def value_function_measure_direct(v_func, o1, o2):
#     """
#     Directly measure difference between the value function of original state and perturbed state.
#     V(o1)-V(o2)
#     """
#     diff  = v_func(o1) - v_func(o2)
#     return np.abs(diff.detach().numpy())

# def estimate_perturbation_effect(env, model, v_func, forward_dynamics, original_input, perturbed_input_list):
#     """
#     Measure the perturbation effects with original input and perturbed inputs for the model, 
#     return the list of effects by perturbation. 
#     """
#     effects_list = []
#     original_output, original_distrib = model(original_input)
#     for perturbed_input in perturbed_input_list:
#         output, distrib = model(perturbed_input)
#         if Importance == 'Policy_diff': # importance value defined as difference of the policies
#             effect = distance_measure(original_output, original_distrib, output, distrib)
#         elif Importance == 'Value_diff': # importance value defined as difference of the values of next state
#             # effect = value_function_measure(env, v_func, forward_dynamics, original_input, original_output, original_distrib, output, distrib)
#             effect = value_function_measure_direct(v_func, original_input, perturbed_input)
#         else:
#             raise NotImplementedError

#         if np.isinf(effect):  # skip the infinite KL value caused by any dim in probability distribution to be 0
#             # print('Infinite Distance!')
#             effects_list.append(np.zeros_like(effect))
#         else:
#             effects_list.append(effect)
#     return effects_list, original_output


# def dt_instance_feature_importance(model, instance, perturbed_features, perturbation_check=False, normalize=True):
#     """
#     Get feature importance on decision tree for single instance with Gini-score reduction
#     """
#     decision_path = model.decision_path([instance])
#     indices_on_path = decision_path.indices

#     left_c = model.tree_.children_left
#     right_c = model.tree_.children_right

#     impurity = model.tree_.impurity    
#     # node_samples = model.tree_.n_node_samples 
#     node_samples = model.tree_.weighted_n_node_samples 
#     node_thresholds = model.tree_.threshold

#     # Initialize the feature importance, those not used remain zero
#     feature_importance = np.zeros((model.tree_.n_features,))

#     node_list = []
#     for idx,node in enumerate(model.tree_.feature):  # 'idx' is the index of tree node, 'node' is the index of feature 
#         SWICH_BRANCH = True  # whether the branch is remained with the perturbed value
#         if perturbation_check:
#             SWICH_BRANCH = False
#             if (instance[node] -  node_thresholds[idx])*(perturbed_features[node] - node_thresholds[idx]) < 0:  
#                 # perturbed value causes a changed numerical relationship wrt the node threshold value, leading to branch switch
#                 SWICH_BRANCH = True
#         if node >= 0 and idx in indices_on_path and SWICH_BRANCH:
#             node_list.append(node)
#             # Accumulate the feature importance over all the nodes where it's used
#             feature_importance[node]+=impurity[idx]*node_samples[idx]- \
#                                    impurity[left_c[idx]]*node_samples[left_c[idx]]-\
#                                    impurity[right_c[idx]]*node_samples[right_c[idx]]
#     # visualize decision path with features on nodes in sequence
#     # print(node_list)

#     # Number of samples at the root node
#     feature_importance/=node_samples[0]

#     if normalize:
#         normalizer = feature_importance.sum()
#         if normalizer > 0:
#             feature_importance/=normalizer

#     return feature_importance

def rollout_perturbation_feature_importance(policy, env, max_epi, max_steps):
    data = load_data(policy, env)
    perturbed_features, features_cov_matrix = get_perturbation_feature_value(data)
    if Perturbation == 'Cluster':
        feature_clusters = perturbed_features[0]
        print('Clusters: ', feature_clusters)
    else:
        feature_clusters=None

    o = env.reset()
    importance_list_in_episode=[max_steps*[o.shape[0]*[0.]]]  # ensure that at least one whole trajectory exists with no nan value
    importance_list_in_timestep = []
    # if Visualize:
    if env.action_space.shape:  # multi-dimension action
        action_dim = env.action_space.shape[0]
    else:
        action_dim = 1
        
    fig, ax_list = plt.subplots(action_dim+2,1, figsize=(8,4*(action_dim+2)))
    ax_list[0].set_title('Action')
    for ax in ax_list[:-3]:
        ax.set_xlabel('Timestep')
    for ax_id in range(len(ax_list[:-2])):
        ax_list[ax_id].set_ylabel('Action Value (Dim {})'.format(ax_id))
    plt.ion()

    if Continuous: 
        Env_type = 'Continuous'
    else:
        Env_type = 'Discrete'

    model_fn = lambda x: policy.choose_action(x, DIST=Stochastic)
    model_v = lambda x: policy.v(x)
    forward_model = ForwardDynamics(env)
    forward_dynamics = lambda x: forward_model.predict(x)

    if Perturbation == 'Single':
        estimation_dimension = o.shape[0]  # not for cluster
    elif Perturbation == 'Cluster':
        estimation_dimension = len(feature_clusters)

    for epi in range(max_epi):
        o = env.reset()
        epi_importance = []
        epi_std = []
        a_list=[]
        # if Visualize:
        last_x=o.shape[0]*[0] # initialization
        last_y=o.shape[0]*[0]
        if Value == 'Shapley':
            last_y_upper=o.shape[0]*[0]
            last_y_lower=o.shape[0]*[0]

        print('Current episode: {}'.format(epi))
        for step in range(max_steps):
            if Value == 'Perturbation':
                perturb_o_list = perturb_features(o.copy(), perturbed_features, estimation_dimension, features_cov_matrix)
                importance_list, a = estimate_perturbation_effect(env, model_fn, model_v, forward_dynamics, o, perturb_o_list)

            elif Value == 'Shapley':
                a, dist = policy.choose_action(o, DIST=Stochastic)
                importance_list=[]
                std_list = []
                for dim in range(estimation_dimension): 
                    mean, std = estimate_shapley(M=Budget, dim=dim, x=o, data=data, f=model_fn, \
                        feature_clusters=feature_clusters, perturbed_features=perturbed_features, \
                        features_cov_matrix=features_cov_matrix)
                    importance_list.append(mean)
                    std_list.append(std)
                            
            elif Value == 'Ground_truth':  # only for decision tree
                a, _= model_fn(o)

                def perturbed_feature_values(intance, perturbed_features, binary_dim):
                    o = perturbed_features.copy()
                    for i in binary_dim:
                        o[i] = 0. if intance[i]>0 else 1. 
                    return o

                perturbed_features_for_instance  =  perturbed_feature_values(o, perturbed_features, binary_dim = [6,7]) # binary_dim for LunarLander only!
                importance_list = dt_instance_feature_importance(policy.tree, o, perturbed_features_for_instance, perturbation_check=True)
                if Perturbation == 'Cluster':
                    importance_list = [np.sum(importance_list[cluster]) for cluster in feature_clusters]
            
            a_list.append(a)
            o_, r, done, _ = env.step(a)
            o = o_


            # normalization and abnormal handling
            if Normalization:
                sum_importance = np.sum(importance_list)
            else:
                sum_importance = 1.
            if sum_importance != 0:
                normalized_importance = np.array(importance_list)/sum_importance
                if Value == 'Shapley':
                    normalized_std_list = np.array(std_list)/sum_importance  # normalize the standard deviation as well
                    epi_std.append(normalized_std_list)
                epi_importance.append(normalized_importance)
                importance_list_in_timestep.append(normalized_importance)
            else:
                epi_importance.append(np.zeros(len(importance_list)))
                if Value == 'Shapley':
                    epi_std.append(np.zeros(len(std_list)))
                importance_list_in_timestep.append(np.zeros(len(importance_list)))
                # print('Sum of Distance is 0!')

            # plotting
            # env.render()
            if Plotting == 'Line':
                # creat colors
                jet= plt.get_cmap('jet')
                n_colors = estimation_dimension
                colors = iter(jet(np.linspace(0,1,n_colors+1)))
                colors_list = [next(colors) for i in range(n_colors)]
                # incremetal plotting for each timestep
                step_interval=Skipped_frames  # for skipping frames to display (speeding up the process)
                ax = ax_list[-2]
                if step>0 and step%step_interval==0:
                    for i in range(estimation_dimension):
                        single_feature = np.array(epi_importance)[:,i]
                        x=np.arange(start=step-1, stop=step+1)
                        y=single_feature[step-1:step+1]
                        ax.plot([last_x[i], x[0]], [last_y[i], y[0]], c=colors_list[i]) # additional plot on skipping interval
                        if Value == 'Shapley':  # plot standard deviation interval
                            single_feature_std = np.array(epi_std)[:,i]
                            y_std=single_feature_std[step-1:step+1]
                            y_upper = y+y_std
                            y_lower = y-y_std
                            # if plot standard deviation interval
                            ax.fill_between(
                                    [last_x[i], x[0]], [last_y_upper[i], y_upper[0]], [last_y_lower[i], y_lower[0]], interpolate=True, facecolor=colors_list[i], linewidth=0.0, alpha=0.3
                                )
                            ax.fill_between(
                            x, list(y_upper), list(y_lower), interpolate=True, facecolor=colors_list[i], linewidth=0.0, alpha=0.3
                        )
                        if Perturbation is 'Cluster':
                            ax.plot(x, y,  label='Cluster {}: Features ({})'.format(i, feature_clusters[i]), c=colors_list[i])
                        else:
                            ax.plot(x, y,  label='Dim {}'.format(i), c=colors_list[i])

                        # plot action list
                        if len(np.array(a_list).shape)>1:  #multi-dimensional action
                            for ax_i, a in zip(ax_list[:-2], np.array(a_list).T):
                                ax_i.scatter(range(len(a)), a, c='black')
                        else:
                            ax_list[-3].scatter(range(len(a_list)), a_list, c='black')

                        if Visualize:
                            plt.pause(0.001)
                        last_x[i] = x[-1]
                        last_y[i] = y[-1]
                        if Value == 'Shapley':  # plot standard deviation interval
                            last_y_upper[i] = y_upper[-1]
                            last_y_lower[i] = y_lower[-1]
                if step/step_interval==1:
                    ax.legend(loc=2)
                ax.set_ylabel('Importance Value')

            elif Plotting == 'Bar':
                plt.cla()
                if step % Skipped_frames==0:
                    grads=np.transpose(epi_importance) # after tranpose: (dimension, N)
                    (dim0, dim1) = grads.shape
                    ax = ax_list[-2]
                    bar = ax.barh(0,1)
                    def gradientbars(bar):
                        ax = bar[0].axes
                        lim = ax.get_xlim()+ax.get_ylim()
                        bar=bar[0]
                        bar.set_zorder(1)
                        bar.set_facecolor("none")
                        x,y = bar.get_xy()
                        w, h = bar.get_width(), bar.get_height()
                        grad = np.atleast_2d(grads)
                        # a hacky way
                        ax.set_yticks([])
                        ax.set_xticks([])
                        n_space = int(60/dim0)
                        ax.set_ylabel("".join([n_space*' '+'{}'.format(i) for i in range(dim0-1, 0-1, -1)])+n_space*' ')
                        im=ax.imshow(grad, extent=[x,x+w,y,y+h], aspect="auto", zorder=0)

                        ax.axis(lim)
                        return im

                    im=gradientbars(bar)

                    # plot color bar
                    # divider = make_axes_locatable(ax)
                    # cax = divider.append_axes('right', size='5%', pad=0.05)
                    # if step>2:
                        # cbar = plt.colorbar(im, cax=cax, orientation='vertical')

                    # plot action list
                    if len(np.array(a_list).shape)>1:  #multi-dimensional action
                        for ax, a in zip(ax_list[:-2], np.array(a_list).T):
                            ax.scatter(range(len(a)), a, c='black')
                    else:
                        ax_list[-3].scatter(range(len(a_list)), a_list, c='black')
                    if Visualize:
                        plt.pause(0.001)
                    
            ax_list[-2].set_title('{} Feature Importance per Timestep ({}, {}, {})'.format(Value, Agent, Perturbation, Env_type))
            ax_list[-2].set_xlabel('Timestep')

            if Visualize:
                plt.imshow(env.render(mode='rgb_array'))
                display.display(plt.gcf())    
                display.clear_output(wait=True)       
            if step%10==0:
                plt.savefig('{}_feature_value-{}-{}-{}.png'.format(Value, Agent, Perturbation, Env_type))
            if done: break
        epi_length = len(epi_importance)
        if epi_length<max_steps:
            for _ in range(max_steps-epi_length):
                epi_importance.append(len(importance_list)*[np.nan])
        importance_list_in_episode.append(epi_importance)
    plt.close()
    env.close()
    return importance_list_in_episode, importance_list_in_timestep

# def average_perturbation_feature_importance(policy, env, max_epi=1000, max_steps=1000, data_file='kl'):
#     """
#     Measure the permuation feature importance averaged over episodes,
#     with perturbation on each input dimension.
#     """
#     importance_list_in_episode, importance_list_in_timestep = rollout_perturbation_feature_importance(policy, env, max_epi, max_steps)
#     if Continuous:
#         data_file+='_continuous'
#     else:
#         data_file+='_discrete'
    
#     data_file+='_'+Agent

#     flatten_data_file = 'flatten_'+data_file

#     np.save(data_file, importance_list_in_episode)
#     np.save(flatten_data_file, importance_list_in_timestep)
#     return importance_list_in_episode

# def plot_final_importance(data_file='kl_discrete_decisiontree20.npy'):
#     data_prefix = './data/'
#     # data_prefix = ''
#     importance_data = np.load(data_prefix+data_file)
#     mean_importance = np.mean(importance_data, axis=0)
#     dim = importance_data.shape[-1]
#     plt.figure(figsize=(8,6))
#     for i in range(dim):
#         single_dim_data = importance_data[:,:, i]
#         x=np.arange(single_dim_data.shape[1])
#         mean_over_episodes = np.nanmean(single_dim_data, 0)  # absent steps are filled with np.nan
#         plt.plot(x, mean_over_episodes, label='Dim {}'.format(i))

#     plt.xlabel('Timestep in Episode')
#     if Stochastic:  
#         plt.ylabel('Average Importance Value (Normalized KL)')
#     else:
#         plt.ylabel('Average Importance Value (Normalized L2-Distance)')
#     leg= plt.legend( loc=1)
#     plt.grid()
#     plt.savefig('importance.png')
#     plt.show()

# def load_agent_model(env,env_type):
#     print(Agent)
#     if env_type == 'minatar':
#         from ppo_seaquest import PPO
#         model = PPO(env.observation_space, env.action_space)
#         model.load_model()

#     elif env_type == 'gym':
#         if Continuous:
#             if Agent=='Heuristic':
#                 model = Heuristic_agent(env, Continuous)
#             elif Agent=='PPO':
#                 from ppo_continuous import PPO
#                 model = PPO(env.observation_space.shape[0], env.action_space.shape[0])
#                 model.load_model()
#             else:
#                 pass
#         else:
#             if Agent=='Heuristic':
#                 model = Heuristic_agent(env, Continuous)
#             elif Agent=='Decision_tree':
#                 model = Decision_tree_agent(env, Continuous, depth=Tree_depth)
#                 print('Feature importances for the tree: ', model.get_feature_importances())
#             elif Agent=='PPO':
#                 from ppo_discrete import PPO
#                 model = PPO(env.observation_space.shape[0], env.action_space.n)
#                 model.load_model()
#             else:
#                 pass
#     else:
#         raise NotImplementedError

#     return model



def policy_analysis():
    import os
    dirpath = os.getcwd()
    foldername = os.path.basename(dirpath)
    if foldername[-6:] != 'gitlab':
        os.chdir('..')  # move current path to one level above
    print("current directory is : " + dirpath)

    env, env_type = make_env(Env_name)
    model = load_agent_model(env, env_type)

    # for reproduciblility
    torch.manual_seed(3)
    env.seed(3)
    np.random.seed(3)

    _, importance_list = rollout_perturbation_feature_importance(model, env, max_epi=1, max_steps=1000)
    average_importances = np.mean(importance_list, axis=0).reshape(-1)
    return average_importances