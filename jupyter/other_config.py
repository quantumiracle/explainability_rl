""" 
Specify other parameters and start running.
"""
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import builtins
import matplotlib.pyplot as plt
import sys
import numpy as np
import os
sys.path.append(".." if os.path.basename(os.getcwd())[-6:] != 'gitlab' else "") 

interact_specify=interact.options(manual=True, manual_name="Run")

# for long desription to display completely
style = {'description_width': 'initial'}
Skipped_frames = widgets.IntSlider(value=2, min=1, max=10, step=1, style=style)

@interact_specify()
def other_configurations(
                        Normalization = True,
                        Visualize = True,  # visualize the env and importance or not
                        Inline = True, # visualize inline or not
                        Plotting = ['Line', 'Bar'], 
                        Skipped_frames = Skipped_frames,    # default value                  
                        # button = widgets.ToggleButton(
                        # value=False,
                        # description='Stop',
                        # icon='check'
                        # ),
                        # toggle = widgets.ToggleButton(description='click me')

    ):

    # choose the env type
    if 'Continuous' in Env_name:
        Continuous = True
    else:
        Continuous = False

    # agent characteristics
    if Agent == 'Heuristic': # heuristic agents are all deterministic
        Stochastic = False
        Heuristic = True
    elif Agent == 'PPO': # PPO and decision-tree are always stochastic, for both discrete and continuous
        Stochastic = True
        Heuristic = False
    elif Agent == 'Decision_tree': 
        if Continuous:
            Stochastic = False
        else: 
            Stochastic = True
        Heuristic = True   # decision-tree is mimicking the heuristic agent 

    R_Norm=0.1
    Gamma = 1.

    print("Other associate configurations: \n Continuous: {}\n Stochastic: {}\n Heuristic: {}\n R_Norm: {}\n Gamma: {}"\
        .format(Continuous, Stochastic, Heuristic, R_Norm, Gamma))
    
    # make configuration parameters global everywhere, as builtins
    builtins.Normalization = Normalization
    builtins.Visualize = Visualize
    builtins.Plotting = Plotting
    builtins.Skipped_frames = Skipped_frames
    builtins.Continuous = Continuous
    builtins.Stochastic = Stochastic
    builtins.Heuristic = Heuristic
    builtins.R_Norm = R_Norm
    builtins.Gamma = Gamma
    builtins.Inline = Inline

    # run it!
    # import policy_analysis_interact  
    if Inline:
        from policy_analysis_interact_inline import policy_analysis
    else:
        from policy_analysis_interact import policy_analysis
    importances = policy_analysis()

    import matplotlib.image as mpimg
    from IPython import get_ipython
    get_ipython().run_line_magic('matplotlib', 'inline')
    if Continuous: 
        Env_type = 'Continuous'
    else:
        Env_type = 'Discrete'
    img=mpimg.imread('{}_feature_value-{}-{}-{}.png'.format(Value, Agent, Perturbation, Env_type))

    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 22))
    ax1.imshow(img, interpolation='nearest')

    width = 0.3
    _ = plt.bar(np.arange(importances.shape[0]), importances, width, label='({}, {}, {}, {})'.format(Value, Agent, Perturbation, Env_type))
    ax2.set_ylabel('Average Feature Importance')
    ax2.set_xlabel('Dimension')
    ax2.legend()

    plt.show()