""" 
Specify method-specific parameters
"""
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import builtins

Characteristic = None
Importance = None
# choose characteristic function for Shapley value
if Value == 'Shapley':  
    @interact(Budget = (10, 1000, 10))
    def specific_configurations1(Characteristic = ['Permutation', 'Perturbation'],
                                Budget = 100,
                                 ):
        builtins.Characteristic = Characteristic
        builtins.Budget = Budget

# choose the defination of importance function
elif Value == 'Perturbation':  
    @interact
    def specific_configurations2(Importance = ['Policy_diff', 'Value_diff']):
        builtins.Importance = Importance

# choose number of clusters
if Perturbation == 'Cluster':
    @interact
    def specific_configurations3(Num_clusters=(1, 10, 1)):
        builtins.Num_clusters = Num_clusters


# choose depth of the tree
if Agent == 'Decision_tree':
    style = {'description_width': 'initial'}
    Tree_depth = widgets.IntSlider(value=20, min=2, max=100, step=1, style=style)
    @interact
    def specific_configurations4(Tree_depth = Tree_depth):
        builtins.Tree_depth = Tree_depth
