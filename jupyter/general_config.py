""" 
Specify general parameters.
"""
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import builtins

@interact
def general_configurations(
                        # Env_name = ['LunarLander-v2','LunarLanderContinuous-v2', 'seaquest'],
                        Env_name = ['LunarLander-v2','LunarLanderContinuous-v2'],
                        Agent = ['PPO', 'Heuristic', 'Decision_tree'],
                        # Perturbation = ['Single', 'Cluster', 'Random'],
                        Perturbation = ['Single', 'Cluster'],
                        Replacement = ['Mode', 'Mean'],
                        Distribution = ['Marginal', 'Conditional'],  # conditional only for mean at present
                        Value = ['Perturbation', 'Shapley', 'Ground_truth'],
                        ):
    builtins.Env_name = Env_name
    builtins.Agent = Agent
    builtins.Perturbation = Perturbation
    builtins.Replacement = Replacement
    builtins.Distribution = Distribution
    builtins.Value = Value
    pass
