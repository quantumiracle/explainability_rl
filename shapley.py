"""
Ref: Štrumbelj, Erik, and Igor Kononenko. 
“Explaining prediction models and individual predictions with feature contributions.” 
Knowledge and information systems 41.3 (2014): 647-665.
"""
import math
import gym
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions import Categorical
import argparse
from minatar import Environment
import numpy as np
import matplotlib.pyplot as plt
import copy
from lunar_lander_heuristic import Heuristic_agent, Decision_tree_agent
from sklearn import cluster
from utils.math import *
# from config import *

def perturb_features_specific(x, perturbed_features, perturbed_dims, features_cov_matrix):
    """
    Perturb the instance on specific dimensions with perturbation values
    """
    binary_dim=[6,7]  # LunarLander 

    perturb_x = x.copy()
    if Perturbation is 'Cluster':
        [feature_clusters,  perturbation_values] = perturbed_features
        if len(perturbed_dims) > 0:
            flatten_feature_clusters = np.concatenate(np.array(feature_clusters))
            flatten_perturbation_values = np.concatenate(np.array(perturbation_values))
            perturbed_feature_dims = np.concatenate(np.array(feature_clusters)[perturbed_dims])
            perturbed_feature_values = np.concatenate(np.array(perturbation_values)[perturbed_dims])
            if Distribution == 'Conditional':
                # get new perturbed feature values from conditional distribution
                marginal_means = flatten_perturbation_values[np.array(flatten_feature_clusters).argsort()]     # sort according to feature order    

                conditioned_dims = [i for i in np.arange(x.shape[0]) if i not in perturbed_feature_dims]
                cov_xx = select_from_matrix(features_cov_matrix, perturbed_feature_dims, perturbed_feature_dims)
                cov_xy = select_from_matrix(features_cov_matrix, perturbed_feature_dims, conditioned_dims)
                cov_yy = select_from_matrix(features_cov_matrix, conditioned_dims, conditioned_dims)

                marginal_means = np.array(marginal_means).reshape(-1,1)
                m_x = marginal_means[perturbed_feature_dims]
                m_y = marginal_means[conditioned_dims]
                v_y = x[conditioned_dims].reshape(-1,1) 
                con_m, con_cov = marginal_to_conditional(m_x, m_y, v_y, cov_xx, cov_xy, cov_yy)
                perturb_x[perturbed_feature_dims] = con_m.reshape(-1)
            else:
                perturb_x[perturbed_feature_dims] = perturbed_feature_values

        # flip value for binary
        for dim in binary_dim:
            if dim in perturbed_dims:
                perturb_x[dim] = 0. if x[dim]>0 else 1.
    else:
        if Distribution == 'Conditional':
            # get new perturbed feature values from conditional distribution
            conditioned_dims = [i for i in np.arange(x.shape[0]) if i not in perturbed_dims]
            cov_xx = select_from_matrix(features_cov_matrix, perturbed_dims, perturbed_dims)
            cov_xy = select_from_matrix(features_cov_matrix, perturbed_dims, conditioned_dims)
            cov_yy = select_from_matrix(features_cov_matrix, conditioned_dims, conditioned_dims)
            perturbed_features = np.array(perturbed_features)
            marginal_means = perturbed_features.reshape(-1,1)  # only work for mean, not mode
            m_x = marginal_means[perturbed_dims]
            m_y = marginal_means[conditioned_dims]
            v_y = x[conditioned_dims].reshape(-1,1) 

            con_m, con_cov = marginal_to_conditional(m_x, m_y, v_y, cov_xx, cov_xy, cov_yy)
            perturbed_features[perturbed_dims] = con_m.reshape(-1)

        for i in perturbed_dims:
            if Perturbation is 'Random': # if random perturbation, replace it with random values from normal distribution
                if i in binary_dim:
                    if np.random.normal()>0.5:
                        perturb_x[i] = 1
                    else:
                        perturb_x[i] = 0
                else:
                    perturb_x[i] = np.random.normal()  # here should be range of state dimension
            elif Perturbation is 'Single':
                if i in binary_dim:  # dim 6, 7, flip for binary
                    perturb_x[i] = 0. if x[i]>0 else 1.
                else:
                    perturb_x[i] = perturbed_features[i]
            else:
                raise NotImplementedError  
    
    return perturb_x

def estimate_shapley(M, dim, x, data, f, feature_clusters=None, perturbed_features=None, features_cov_matrix=None):
    """
    Estimate the Shapley value through instances permutation, rather than feature perturbation.
    Arguments:
    M: number of iterations
    dim: feature dimension for shapley value estimation
    x: instance of interest
    data: dataset of instances
    f: model to interpret
    feature_clusters: if not None, it's a list of indices clusters for features
    """
    feature_dim = len(feature_clusters) if feature_clusters else np.array(data).shape[-1]
    all_dim_list = np.arange(np.array(data).shape[-1])
    idx = np.arange(feature_dim)
    marginal_contrib_list = []
    for itr in range(M):
        shuffled_idx = idx.copy()
        np.random.shuffle(shuffled_idx)
        shuffled_idx_for_dim = int(np.where(shuffled_idx==dim)[0]) # only the first one (the only one)
        if shuffled_idx_for_dim<feature_dim:
            replaced_dims_plus = shuffled_idx[shuffled_idx_for_dim+1:]  # [...., |dim+1, dim+2, ..., N] to be replaced
        else:
            replaced_dims_plus=[]
        replaced_dims_minus = shuffled_idx[shuffled_idx_for_dim:]   # [...., | dim, dim+1, dim+2, ..., N] to be replaced

        def characteristic_function_permutation(x, instance, f, replaced_dims):
            """
            Characteristic function in Shapley value estimation: instance permutation.
            For each possible feature subset sampled for estimating Shapley, replace the features not in the 
            subsets with the corresponding feature values from another randomly sampled instance in pre-collected
            datatset.

            Arguments
            ----------
            x: input to be explained
            instance: a sample in pre-collected data
            f: the model
            """
            j = x.copy()
            j[replaced_dims] = instance[replaced_dims]
            return f(j)

        def characteristic_function_perturbation(x, f, perturbed_features, replaced_dims):
            """
            Characteristic function in Shapley value estimation: feature value perturbation.
            For each possible feature subset sampled for estimating Shapley, replace the features not in the 
            subsets with the corresponding statistical values (mode, mean, etc) of pre-collected dataset.
            datatset.

            Arguments
            ----------            
            x: instance
            f: the model
            perturbed_features: feature values after perturbation
            """
            # perturb the instance
            perturbed_x = perturb_features_specific(x, perturbed_features, replaced_dims, features_cov_matrix)
            return f(perturbed_x)

        if Characteristic == 'Permutation':
            data_size = np.array(data).shape[0]
            # randomly sampled instance z from dataset
            sample_size=1
            sample_idx = np.random.randint(data_size, size=sample_size)
            instance = data[sample_idx]
            if sample_size==1:
                instance = instance.reshape(-1)

            # the indices are for feature clusters when features are clustered, so transfer into features indices first
            if feature_clusters:  
                replaced_clusters_plus = [ feature_clusters[dim] for dim in replaced_dims_plus]
                replaced_clusters_minus = [ feature_clusters[dim] for dim in replaced_dims_minus]

                flatten = lambda l: [item for sublist in l for item in sublist]  # flatten list of list to list
                replaced_dims_plus = flatten(replaced_clusters_plus)
                replaced_dims_minus = flatten(replaced_clusters_minus)

            v0, dist0 = characteristic_function_permutation(x, instance, f, all_dim_list)  # replace all features as baseline
            v1, dist1 = characteristic_function_permutation(x, instance, f, replaced_dims_plus)
            v2, dist2 = characteristic_function_permutation(x, instance, f, replaced_dims_minus)

        elif Characteristic == 'Perturbation':
            if feature_clusters:
                v0, dist0 = characteristic_function_perturbation(x, f, perturbed_features,  np.arange(len(feature_clusters)))
            else:
                v0, dist0 = characteristic_function_perturbation(x, f, perturbed_features,  all_dim_list)
            v1, dist1 = characteristic_function_perturbation(x, f, perturbed_features,  replaced_dims_plus)
            v2, dist2 = characteristic_function_perturbation(x, f, perturbed_features,  replaced_dims_minus)
        
        # marginal contribution = f(j_plus) - f(j_minus)
        # print(v1, dist1, v0, dist0)
        d1 = distance_measure(v1, dist1, v0, dist0)  # defination of Shapley value
        d2 = distance_measure(v2, dist2, v0, dist0)
        # print(d1, d2)
        if np.isinf(d1) or np.isinf(d2):
            marginal_contrib = np.zeros_like(d1)
        else:
            marginal_contrib = np.abs(d1-d2)  # ensure non-negative
        # marginal_contrib = distance_measure(v1, dist1, v2, dist2)  # a novel one: characteristic function outputs distribution
        marginal_contrib_list.append(marginal_contrib)

        # since we get a bunch of samples here, we can get any statistics for the estimated Shapley value
        Shapley_mean = np.mean(marginal_contrib_list)  # no need to divide the factorial since it's averaged here
        Shapley_std = np.std(marginal_contrib_list)
        # print(Shapley_mean-Shapley_std)
    return Shapley_mean, Shapley_std

