""" 
Specify global variables
"""
# choose the env
Env_name = ['LunarLander-v2','LunarLanderContinuous-v2', 'seaquest'][1]

# choose an agent type
Agent = ['Heuristic', 'PPO', 'Decision_tree'][1]

# choose the env type
if 'Continuous' in Env_name:
    Continuous = True
else:
    Continuous = False

# agent characteristics
if Agent == 'Heuristic': # heuristic agents are all deterministic
    Stochastic = False
    Heuristic = True
elif Agent == 'PPO': # PPO and decision-tree are always stochastic, for both discrete and continuous
    Stochastic = True
    Heuristic = False
elif Agent == 'Decision_tree': 
    Stochastic = True
    Heuristic = True   # decision-tree is mimicking the heuristic agent 

# choose perturbation methods
Perturbation = ['Random', 'Single', 'Cluster'][2]
if Perturbation == 'Cluster':
    Num_clusters = 5

# choose feature value replacement methods
Replacement = ['Mode', 'Mean'][0]

# choose feature importance value estimation methods
Value = ['Perturbation', 'Shapley'][0]  # feature value estimation method: perturbation feature value or Shapley value
if Value == 'Shapley':  # choose characteristic function for Shapley value
    Characteristic = ['Permutation', 'Perturbation'][1]
elif Value == 'Perturbation':  # choose the defination of importance function
    Importance = ['Policy_diff', 'Value_diff'][1]  # PPO has value function, 'Value diff' for PPO only

# choose visualizing the importance or not
Visualize = True # visualize the env.step() and importance value


# choose visualization method
if Visualize:
    Plotting = ['Line', 'Bar'][0]

# normalize the importance or not
# when the importance values are all positive, they should be normalized; when some are
# negative, it's not proper to normalize them.
Normalization = False

R_Norm=0.1
Gamma = 1.


