import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import gym
import argparse
from config import Gamma

ModelPath = './model/policy_estimation'

parser = argparse.ArgumentParser(description='Train or test neural net motor controller.')
parser.add_argument('--train', dest='train', action='store_true', default=False)
parser.add_argument('--test', dest='test', action='store_true', default=False)
args = parser.parse_args()


def rollout(env, policy, N, max_steps=1000):
    o_list = []
    params_list = []  # policy parameters
    o__list = []
    r_list=[]
    for epi in range(N):
        o = env.reset()
        for _ in range(max_steps):
            mean, std = policy(torch.Tensor(o).unsqueeze(0).cuda())
            pi = torch.distributions.Normal(mean, std)
            a = pi.sample().squeeze(0).detach().cpu().numpy()
            o_,r,done,_ = env.step(a)
            o_list.append(o)
            params_list.append(np.concatenate((mean.squeeze(0).detach().cpu().numpy(), std.squeeze(0).detach().cpu().numpy() )))
            o__list.append(o_)
            r_list.append([r])
            o = o_
            if done:
                break

    return o_list, params_list, o__list, r_list
        

class PolicyEstimation(nn.Module):
    def __init__(self, env):
        super(PolicyEstimation, self).__init__()
        a_dim = env.action_space.shape[0]
        o_dim = env.observation_space.shape[0]
        input_dim = o_dim + 2*a_dim
        output_dim = 1
        hidden_dim = 128
        self.fc1   = nn.Linear(input_dim,hidden_dim)
        self.fc2   = nn.Linear(hidden_dim,hidden_dim)
        self.fc3   = nn.Linear(hidden_dim,hidden_dim)
        self.fc4   = nn.Linear(hidden_dim,output_dim)

        self.env = env
        self.optimizer = optim.Adam(self.parameters(), lr=1e-4)

    def predict(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        output = self.fc4(x)
        
        return output


    def learn(self, policy, value, epoch = 100, itr=10,  size=5):
        for ep in range(epoch):
            o, p, o_, r = rollout(env, policy, size)
            op = np.concatenate((o,p), axis=1)
            self.criterion = torch.nn.MSELoss()
            o_ = torch.Tensor(o_)
            r = torch.Tensor(r)

            for _ in range(itr):
                pre_q = self.predict(torch.Tensor(op))
                v_ = value(o_.cuda()).detach().cpu()
                q = r + Gamma*v_
                loss = self.criterion(pre_q, q)
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
            print('Epoch: {}  Loss: {}'.format(ep, loss))

        self.save_model(ModelPath)
        return loss

    def save_model(self, path=ModelPath): 
        torch.save(self.state_dict(), path)

    def load_model(self, path=ModelPath):
        self.load_state_dict(torch.load(path))


def test(env, policy, policy_estimation):
    o, p, o_, r = rollout(env, policy, N=1)
    op = torch.Tensor(np.concatenate((o, p), axis=1))
    _ = policy_estimation.predict(op).detach().cpu().numpy()
    

if __name__ == '__main__':
    env = gym.make('LunarLanderContinuous-v2')
    from ppo_continuous_multiprocess2 import PPO
    model = PPO(env.observation_space.shape[0], env.action_space.shape[0])
    model.load_model()
    policy = lambda x: model.actor(x)  # return mean and std
    value = lambda x: model.critic(x)
    policy_estimation = PolicyEstimation(env)

    if args.train:
        policy_estimation.learn(policy, value)
    
    if args.test:
        policy_estimation.load_model()
        test(env, policy, policy_estimation)
