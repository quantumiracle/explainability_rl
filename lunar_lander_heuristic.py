import sys, math
import numpy as np

import Box2D
from Box2D.b2 import (edgeShape, circleShape, fixtureDef, polygonShape, revoluteJointDef, contactListener)

import gym
from gym import spaces
from gym.utils import seeding, EzPickle
from torch.distributions import Categorical
import torch
from sklearn import tree
from joblib import dump, load

class Heuristic_agent():

    def __init__(self, env, Continuous):
        super(Heuristic_agent, self).__init__()
        self.continuous = Continuous
    
    def choose_action(self, s,  DIST = False):
        # Heuristic for:
        # 1. Testing. 
        # 2. Demonstration rollout.
        angle_targ = s[0]*0.5 + s[2]*1.0         # angle should point towards center (s[0] is horizontal coordinate, s[2] hor speed)
        if angle_targ >  0.4: angle_targ =  0.4  # more than 0.4 radians (22 degrees) is bad
        if angle_targ < -0.4: angle_targ = -0.4
        hover_targ = 0.55*np.abs(s[0])           # target y should be proporional to horizontal offset

        # PID controller: s[4] angle, s[5] angularSpeed
        angle_todo = (angle_targ - s[4])*0.5 - (s[5])*1.0
        #print("angle_targ=%0.2f, angle_todo=%0.2f" % (angle_targ, angle_todo))

        # PID controller: s[1] vertical coordinate s[3] vertical speed
        hover_todo = (hover_targ - s[1])*0.5 - (s[3])*0.5
        #print("hover_targ=%0.2f, hover_todo=%0.2f" % (hover_targ, hover_todo))

        if s[6] or s[7]: # legs have contact
            angle_todo = 0
            hover_todo = -(s[3])*0.5  # override to reduce fall speed, that's all we need after contact

        if self.continuous:
            a = np.array( [hover_todo*20 - 1, -angle_todo*20] )
            a = np.clip(a, -1, +1)
        else:
            a = 0  # do nothing
            if hover_todo > np.abs(angle_todo) and hover_todo > 0.05: a = 2  # fire main
            elif angle_todo < -0.05: a = 3  # fire right
            elif angle_todo > +0.05: a = 1  # fire left
        return a, None



def select_action_from_node(value_list):
    # normalize
    total = np.sum(value_list)
    if total>0:
        prob = np.array(value_list)/float(total)
    dist = Categorical(torch.from_numpy(prob))
    a = dist.sample().item()
    return a, dist



class Decision_tree_agent():  # discrete; not work well
    def __init__(self, env, Continuous, depth=None):
        super(Decision_tree_agent, self).__init__()
        self.continuous = Continuous
        self.depth = depth
        self.model_prefix = './model/continuous_' if self.continuous else './model/discrete_'
        try:  # if tree model exists, load it; else, fit one.
            self.tree = load(self.model_prefix+'decision_tree{}.joblib'.format(self.depth))
            print('Tree loaded! Depth: ', self.depth)
        except:
            self.fit_model()

    def fit_model(self):
        data_prefix = './data/continuous_' if self.continuous else './data/discrete_'
        s = np.load(data_prefix+'state.npy')
        a = np.load(data_prefix+'action.npy')
        if self.continuous:
            model = tree.DecisionTreeRegressor(max_depth=self.depth)
        else:
            model = tree.DecisionTreeClassifier(max_depth=self.depth)
        a = a.reshape(a.shape[0], -1)

        self.tree = model.fit(s, a)
        print('Tree fitted! Depth: ', self.tree.get_depth())
        dump(self.tree, self.model_prefix+'decision_tree{}.joblib'.format(self.depth))
        
    def choose_action(self, s, DIST = False):
        s = np.array(s).reshape(1, -1)
        a = self.tree.predict(s)     
        if self.continuous:
            return a.reshape(-1), None 
        else:
            a = int(a)
            if DIST:
                prob = self.tree.predict_proba(s).reshape(-1)
                if 0 in prob:
                    prob = prob + 1e-20 # remove value 0 in categorical distribution, which can cause infinite KL-divergence
                dist = Categorical(torch.from_numpy(prob))
                return a, dist
            else:
                return a, None

    def get_feature_importances(self):
        return self.tree.feature_importances_


def demo_heuristic_lander(env, heuristic_agent, seed=None, render=False):
    """
    Collect demonstrations.
    """
    env.seed(seed)
    total_reward_list=[]
    a_list=[]
    s_list=[]
    for i in range(10000):
        print('Episode: ', i)
        total_reward = 0
        steps = 0
        s = env.reset()
        while steps < 1000:
            a, _ = heuristic_agent.choose_action(s)
            s_list.append(s)
            a_list.append([a])
            s, r, done, info = env.step(a)
            total_reward += r

            if render:
                still_open = env.render()
                if still_open == False: break

            # if steps % 20 == 0 or done:
            #     print("observations:", " ".join(["{:+0.2f}".format(x) for x in s]))
            #     print("step {} total_reward {:+0.2f}".format(steps, total_reward))
            steps += 1
            if done: break
        total_reward_list.append(total_reward)  
    print('Average reward: {}'.format(np.mean(total_reward_list)))
    np.save('state', s_list)
    np.save('action', a_list)
    return total_reward


if __name__ == '__main__':
    Continuous = True
    if Continuous:
        env = gym.make('LunarLanderContinuous-v2').unwrapped
    else:
        env = gym.make('LunarLander-v2').unwrapped
    heuristic_agent = Heuristic_agent(env, Continuous)
    # heuristic_agent = Simple_heuristic_agent(env, Continuous)
    # heuristic_agent = Decision_tree_agent(env, Continuous, depth=20)
    demo_heuristic_lander(env, heuristic_agent, render=False)
    # data = np.load('sa.npy', allow_pickle=True)
    # print(np.array(data[0]).shape, np.array(data[1]).shape)
    
    
