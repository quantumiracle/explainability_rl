import numpy as np
import torch
import gym
from utils.gym_wrapper import GymWrapper
from gym import envs


def make_env(env_name):
    gym_envs = envs.registry.all()
    gym_env_ids = [env_spec.id for env_spec in gym_envs]

    if env_name in gym_env_ids:  # Gym envs
        env = gym.make(env_name)
        Env_type = 'gym'
    else:  # MinAtar envs
        env = GymWrapper(Environment(env_name))
        Env_type = 'minatar'
    return env, Env_type
