import numpy as np
import torch

__all__=['kl_measure',
        'l2_norm',
        'distance_measure',
        'marginal_to_conditional',
        'select_from_matrix',
        ]

def kl_measure(dist1, dist2):
    """
    Measure KL-divergence of two distributions (Diagonal Gaussians, or Categorical), 
    return scalar KL value.
    """
    kl =  torch.distributions.kl.kl_divergence(dist1, dist2)
    try:
        kl = float(kl.detach().numpy())
    except:
        if kl.shape[-1]>1: # multi-variate
            kl = torch.sum(kl, dim=-1)  # KL of independent multivariates is sum of individual KL: https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence
        kl = float(kl.detach().cpu())  
    kl = np.clip(kl, 0, np.inf)  # numerical instability may give negative KL-divergence value
    return kl

def l2_norm(v1, v2):
    return np.linalg.norm(v1-v2)

def distance_measure(v1, dist1, v2, dist2):
    """
    Measure the distance of two scalar-vectors or distributions
    """
    if dist1 and dist2:
        distance = kl_measure(dist1, dist2)
    else:
        distance = l2_norm(v1, v2)
    return distance


def marginal_to_conditional(m_x, m_y, v_y, cov_xx, cov_xy, cov_yy):
    '''
    Get conditional distribution with the marginal distribution for single instance,
    with the assumption of multivariate normal Gaussian distribution.
    P(x|y)
    
    Arguments:
    -----------
    m_x: marginal mean of x;
    m_y: marginal mean of y;
    v_y: value of y to be conditioned on;
    cov_xx: covariance matrix of x and x;
    cov_xy: covariance matrix of x and y;
    cov_yy: covariance matrix of y and y;
    
    Return:
    -----------
    con_m_x: conditional mean of x;
    con_cov_x: condtional covariance matrix of x;
    '''
    con_m_x = m_x + cov_xy.dot(np.linalg.inv(cov_yy)).dot(v_y - m_y)
    con_cov_x = cov_xx - cov_xy.dot(np.linalg.inv(cov_yy)).dot(cov_xy.T)
    return con_m_x, con_cov_x


def select_from_matrix(matrix, dim0, dim1):
    """
    Select items in matrix with rows and columns, for example:
    a=np.array([[1,2,3], [4,5,6], [7,8,9]])
    dim0=[1,2]
    dim1=[0]
    print(a[np.ix_(dim0, dim1)])
    => [[4] [7]]
    """
    
    return matrix[np.ix_(dim0, dim1)]