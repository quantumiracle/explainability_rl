import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import gym
import argparse
from config import R_Norm


ModelPath = './model/forward_dynamics'

def rollout(env, policy, N, max_steps=1000):
    o_list = []
    a_list = []
    r_o__list = []
    r_list = []
    for epi in range(N):
        o = env.reset()
        for _ in range(max_steps):
            a = policy(o)
            o_,r,done,_ = env.step(a)
            o_list.append(o)
            a_list.append(a)
            r_o__list.append( np.concatenate(([R_Norm*r], o_)) )
            o = o_
            if done:
                break

    return o_list, a_list, r_o__list
        

class ForwardDynamics(nn.Module):
    def __init__(self, env):
        super(ForwardDynamics, self).__init__()
        a_shape = env.action_space.shape or env.action_space.n
        if isinstance(a_shape, tuple):
            a_dim = a_shape[0]
        else:
            a_dim = a_shape
        o_dim = env.observation_space.shape[0]
        r_dim=1
        input_dim = o_dim + a_dim
        output_dim = o_dim + r_dim
        hidden_dim = 128
        self.fc1   = nn.Linear(input_dim,hidden_dim)
        self.fc2   = nn.Linear(hidden_dim,hidden_dim)
        self.fc3   = nn.Linear(hidden_dim,hidden_dim)
        self.fc4   = nn.Linear(hidden_dim,output_dim)

        self.env = env
        self.optimizer = optim.Adam(self.parameters(), lr=1e-4)

    def predict(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        output = self.fc4(x)
        
        return output


    def learn(self, policy, epoch = 100, itr=10,  size=5):
        for ep in range(epoch):
            o, a, r_o_ = rollout(env, policy, size)
            oa = np.concatenate((o,a), axis=1)
            self.criterion = torch.nn.MSELoss()
            r_o_ = torch.Tensor(r_o_)

            for _ in range(itr):
                pre_r_o_ = self.predict(torch.Tensor(oa))
                loss = self.criterion(pre_r_o_, r_o_)
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
            print('Epoch: {}  Loss: {}'.format(ep, loss))

        self.save_model(ModelPath)
        return loss

    def save_model(self, path=ModelPath): 
        torch.save(self.state_dict(), path)

    def load_model(self, path=ModelPath):
        self.load_state_dict(torch.load(path))


def test(env, policy, forward_model):
    o, a, r_o_ = rollout(env, policy, N=1)
    oa = torch.Tensor(np.concatenate((o, a), axis=1))
    pre_r_o_ = forward_model.predict(oa).detach().cpu().numpy()
    print(r_o_[:5], pre_r_o_[:5])
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train or test neural net motor controller.')
    parser.add_argument('--train', dest='train', action='store_true', default=False)
    parser.add_argument('--test', dest='test', action='store_true', default=False)
    args = parser.parse_args()

    env = gym.make('LunarLanderContinuous-v2')
    from ppo_continuous_multiprocess2 import PPO
    model = PPO(env.observation_space.shape[0], env.action_space.shape[0])
    model.load_model()
    policy = lambda x: model.choose_action(x)
    forward_model = ForwardDynamics(env)

    if args.train:
        forward_model.learn(policy)
    
    if args.test:
        forward_model.load_model()
        test(env, policy, forward_model)
